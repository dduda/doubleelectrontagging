#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "AnaAlgorithm/AnaAlgorithm.h"
///#include "SystematicsHandles/SysReadHandle.h"
///#include "SystematicsHandles/SysListHandle.h"
#include "TLorentzVector.h"
#include <string>
#include <iostream>
#include "TTree.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TEfficiency.h"


namespace TMVA{
  class Reader;
}

class MyxAODAnalysis : public EL::AnaAlgorithm
{

public:
   xAOD::TEvent *m_event;  //!

public:
   std::string method; //!
   TTree *m_myTree; //!
   TMVA::Reader *m_mvaReader; //!
   int m_eventCounter; //!
   float m_FatElectron_pT; //!
   float m_FatElectron_Eta; //!
   float m_FatElectron_M; //!
   float m_FatElectron_Eratio; //!
   float m_FatElectron_balance; //!
   int   m_FatElectron_ntrks; //!
   int   m_ChargeSum; //!
   int   m_type; //!
   float m_Trk1_pT; //!
   float m_Trk2_pT; //!
   float m_Trk1_dRToJet; //!
   float m_Trk2_dRToJet; //!
   float m_Trk1_d0; //!
   float m_Trk2_d0; //!
   float m_Trk1_z0; //!
   float m_Trk2_z0; //!
   float m_Trk1_d0Sig; //!
   float m_Trk2_d0Sig; //!
   float m_Trk1_z0Sig; //!
   float m_Trk2_z0Sig; //!
   float m_Trk1_EOverP; //!
   float m_Trk2_EOverP; //!
   float m_Trk1_dPhi; //!
   float m_Trk2_dPhi; //!
   float m_Trk1_dEta; //!
   float m_Trk2_dEta; //!
   float m_Trk1_eProbabilityHT; //!
   float m_Trk2_eProbabilityHT; //!
   float m_dR_tt; //!
   float m_nVertices; //!
 

   // this is a standard constructor
   MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
   
   // these are the functions inherited from Algorithm
   virtual StatusCode initialize () override;
   ////virtual StatusCode histInitialize ();
   virtual StatusCode execute () override;
   virtual StatusCode finalize () override;


   void Sort_trks(std::vector<TLorentzVector> *trksInJet, std::vector<int> *trksIndices);
protected:

  StatusCode FindDoubleElectronCandidate();
};

#endif
