#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "UserAnalysis::DoubleElectronTaggingLib" for configuration "RelWithDebInfo"
set_property(TARGET UserAnalysis::DoubleElectronTaggingLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(UserAnalysis::DoubleElectronTaggingLib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libDoubleElectronTaggingLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libDoubleElectronTaggingLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS UserAnalysis::DoubleElectronTaggingLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_UserAnalysis::DoubleElectronTaggingLib "${_IMPORT_PREFIX}/lib/libDoubleElectronTaggingLib.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
