#include "TH2F.h"
#include "TFile.h"
#include "TList.h"
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class TH2Fs {
  
    map<TString,TH2F*> m_histos_pt_int;
    TString m_name;
    TString m_title;
    vector<double> m_intervals;
    map<int,TString> m_interval_name_index;
 

 public:
    
    TH2Fs(){};
    
    TH2Fs(TString histname, TString title,int nybins, double ylow, double yup ,int nxbins, double xlow, double xup, vector<TString> sel, vector<double> intervals){
        m_intervals = intervals;        
        //Histograms in pT bins
        vector<TString> names_pt_int;
        for(unsigned int i=0; i<intervals.size(); i++){
            TString int_name = ""; 
            int_name += intervals.at(i);
            if(i<intervals.size()-1) {
                int_name+="_";
                int_name+=intervals.at(i+1);
            }
            m_interval_name_index[i] = int_name;
            for (auto s : sel){
                names_pt_int.push_back(int_name+"_"+s);
            }
        }
        TString appo_name_pt_int;
        TString appo_title_pt_int;
        for(int i=0; i<names_pt_int.size(); i++){
            appo_name_pt_int = histname + "_" + names_pt_int.at(i);
            TH2F * h_appo_pt_int =new TH2F(appo_name_pt_int,"", nybins, ylow, yup ,nxbins, xlow, xup);
            m_histos_pt_int[names_pt_int.at(i)]=(TH2F*)h_appo_pt_int->Clone();
            m_histos_pt_int[names_pt_int.at(i)]->SetDefaultSumw2();
            delete h_appo_pt_int;
        }
    }
    
  void Fill(double val_x, double val_y, double weight, TString m_Type, int int_index);	
  void Write();
};

