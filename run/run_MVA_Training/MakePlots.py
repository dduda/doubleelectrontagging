# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
from ROOT import *
from array import *

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../AtlasStyle.C")
gROOT.LoadMacro("../AtlasUtils.C")
SetAtlasStyle()

y_axis_label ="Event fraction"
YAxisScale   = 1.2

c_blue    = TColor.GetColor("#3366ff")
c_red     = TColor.GetColor("#ee3311")
c_orange  = TColor.GetColor("#ff9900")

file      = TFile.Open("FatElectronHistos.root","READ")
file.ls()

XaxisLabel = {}
XaxisLabel["FatElectron_EMFrac"] = "EM fraction"
XaxisLabel["h_DR_tracks"]        = "#DeltaR(trk_{1},trk_{2})"
XaxisLabel["h_Ntrks"]            = "Number of GSF Tracks"
XaxisLabel["FatElectron_Eratio"] = "EnergyRatio"
XaxisLabel["h_FatElectron_balance"] = "FatElectron balance" 
XaxisLabel["h_Trk1_dRToJet"]     = "#DeltaR(trk_{1},jet)"
XaxisLabel["h_Trk2_dRToJet"]     = "#DeltaR(trk_{2},jet)"
XaxisLabel["h_Trk1_d0"]          = "Trk_{1} |d_{0}|"
XaxisLabel["h_Trk2_d0"]          = "Trk_{2} |d_{0}|"
XaxisLabel["h_Trk1_z0"]          = "Trk_{1} |z_{0}|"
XaxisLabel["h_Trk2_z0"]          = "Trk_{2} |z_{0}|"
XaxisLabel["h_Trk1_d0Sig"]       = "Trk_{1} d_{0} significance"
XaxisLabel["h_Trk2_d0Sig"]       = "Trk_{2} d_{0} significance"
XaxisLabel["h_Trk1_z0Sig"]       = "Trk_{1} z_{0} significance"
XaxisLabel["h_Trk2_z0Sig"]       = "Trk_{2} z_{0} significance"
XaxisLabel["h_Trk1_EOverP"]      = "Trk_{1} E/P"
XaxisLabel["h_Trk2_EOverP"]      = "Trk_{2} E/P"
XaxisLabel["h_Trk1_dPhi"]        = "Trk1_dPhi"
XaxisLabel["h_Trk2_dPhi"]        = "Trk2_dPhi"
XaxisLabel["h_Trk1_dEta"]        = "Trk1_dEta"
XaxisLabel["h_Trk2_dEta"]        = "Trk2_dEta"
XaxisLabel["Trk1_eProbabilityHT"] = "Trk1_eProbabilityHT"
XaxisLabel["Trk2_eProbabilityHT"] = "Trk2_eProbabilityHT"
XaxisLabel["h_BDTresponse"]     = "BDT response"

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

for HistoName in ["h_BDTresponse","FatElectron_EMFrac","Trk2_eProbabilityHT","Trk1_eProbabilityHT","h_Trk1_dEta","h_Trk2_dEta","h_Trk1_dPhi","h_Trk2_dPhi","h_Trk1_EOverP","h_Trk2_EOverP","h_Trk2_d0Sig","h_Trk1_d0Sig","h_Trk1_z0Sig","h_Trk2_z0Sig","h_Trk1_z0","h_Trk2_z0","h_Trk1_d0","h_Trk2_d0","h_Trk2_dRToJet","h_Trk1_dRToJet","h_DR_tracks","h_Ntrks","FatElectron_Eratio","h_FatElectron_balance"]:
  for pTRange in ["1000_2000","500_1000","300_500"]:
      Xaxis_label=XaxisLabel[HistoName]
      h_Zee = file.Get(HistoName+"_"+pTRange+"_Zee")
      h_Zee.SetLineColor(kBlue)

      h_QCD = file.Get(HistoName+"_"+pTRange+"_QCDjets")
      h_QCD.SetLineColor(kRed)
      h_QCD.SetLineStyle(5)

      h_Photon = file.Get(HistoName+"_"+pTRange+"_Photonjets")
      h_Photon.SetLineColor(kViolet)
      h_Photon.SetLineStyle(6)

      h_ejets = file.Get(HistoName+"_"+pTRange+"_ejets")
      h_ejets.SetLineColor(kGreen+3)
      h_ejets.SetLineStyle(3)

      nbins=20
      ymax=0
      NormalizeHisto(h_QCD)
      if ymax<h_QCD.GetMaximum():
         ymax=h_QCD.GetMaximum()
      NormalizeHisto(h_Zee)
      if ymax<h_Zee.GetMaximum():
         ymax=h_Zee.GetMaximum()
      NormalizeHisto(h_Photon)
      if ymax<h_Photon.GetMaximum():
         ymax=h_Photon.GetMaximum()
      NormalizeHisto(h_ejets)
      if ymax<h_ejets.GetMaximum():
         ymax=h_ejets.GetMaximum()
  
      h_Zee.GetYaxis().SetRangeUser(0.001,round(ymax*YAxisScale,3));

      h_Zee.GetXaxis().SetTitle(Xaxis_label)
      h_Zee.GetYaxis().SetTitle(y_axis_label)
      c1 = TCanvas("ShapePlots","",720,720)
      h_Zee.Draw("HIST")
      h_QCD.Draw("HISTSAME")
      h_Photon.Draw("HISTSAME")
      h_ejets.Draw("HISTSAME")
      h_Zee.Draw("HISTSAME")

      leg = TLegend(0.25,0.65,0.55,0.80)
      leg2 = TLegend(0.55,0.65,0.85,0.80)
      pT_Range = pTRange.replace("_",",")
      
      myText(0.20,0.825,1,"#sqrt{s}=13 TeV   p_{T} #in  [" + pT_Range + "] GeV")
      ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.19);
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      leg2.SetShadowColor(kWhite)
      leg2.SetFillColor(kWhite)
      leg2.SetLineColor(kWhite)
      leg.AddEntry(h_Zee,     "Z#rightarrowee-jets","L")
      leg.AddEntry(h_QCD,     "QCD-jets     ","L")
      leg2.AddEntry(h_Photon, "#gamma-jets  ","L")
      leg2.AddEntry(h_ejets,  "single e-jets","L")
      leg.Draw()
      leg2.Draw()

      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("Plots/ShapeComparisonPlot_%s.pdf" % (HistoName+"_"+pTRange))

