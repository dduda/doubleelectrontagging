#include "TEfficiency.h"
#include "TFile.h"
#include "TList.h"
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class TEfficiencies {
  
    map<TString,TEfficiency*> m_efficiencies;
    TString m_name;
    TString m_title;
 
 public:
    
    TEfficiencies(){};
    
    TEfficiencies(TString histname, TString title, int nxbins, double* xbins, vector<TString> types){
       TString add_name;
       for(int i=0; i< types.size(); i++){
            add_name                    =  histname + "_" + types.at(i);
            TEfficiency *eff_add        =  new TEfficiency(add_name, "", nxbins, xbins);
            m_efficiencies[types.at(i)] =  (TEfficiency*) eff_add->Clone();
            delete eff_add;
       } 
    }
    
    void Fill(bool passed, double x_value, double weight, TString type);	
    void Write();
};
