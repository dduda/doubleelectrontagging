from ROOT import TH1F
from ROOT import TCanvas
from ROOT import TLine
from ROOT import gStyle
from ROOT import TGraph
from ROOT import *
import re
import sys
import glob
import math
from array import *

gROOT.LoadMacro("../AtlasStyle.C")
gROOT.LoadMacro("../AtlasUtils.C")
SetAtlasStyle()

def Set_Graph_Style(graph, color, marker_style):
    graph.SetMarkerStyle(marker_style)
    graph.SetMarkerSize(1.8)
    graph.SetMarkerColor(color)
    graph.SetLineWidth(2)
    graph.SetLineColor(color)

file = TFile.Open("FatElectronHistos.root","READ")
file.ls()

for Observable in ["pT","Eta","nVtx"]:
 for Process in ["Photonjets","QCDjets","Zee","ejets"]:
   canv = TCanvas("Eff"+Observable+Process,"",1)
   canv.SetBatch(True)
   canv.SetLogy()

   teff_loose  = file.Get("Efficiency_vs_"+Observable+"_loose_"+Process)
   teff_medium = file.Get("Efficiency_vs_"+Observable+"_loose_"+Process)
   teff_tight  = file.Get("Efficiency_vs_"+Observable+"_loose_"+Process)

   teff_loose.Paint("")
   teff_medium.Paint("")
   teff_tight.Paint("")
   Set_Graph_Style(teff_loose.GetPaintedGraph(),kBlack,22)
   Set_Graph_Style(teff_tight.GetPaintedGraph(),kRed,23)
   Set_Graph_Style(teff_medium.GetPaintedGraph(),kBlue,24)
   teff_loose.GetPaintedGraph().GetXaxis().SetTitle("FatElectron p_{T} [GeV]")
   if "Eta" in Observable:
        teff_loose.GetPaintedGraph().GetXaxis().SetTitle("Electron |#eta|")
   if "nVtx" in Observable:
        teff_loose.GetPaintedGraph().GetXaxis().SetTitle("Number of pile-up vertices")

   if "QCDjets" in Process:
      teff_loose.GetPaintedGraph().GetYaxis().SetRangeUser(0.0001,1)
   else: 
      teff_loose.GetPaintedGraph().GetYaxis().SetRangeUser(0.037,27)
   teff_loose.GetPaintedGraph().GetYaxis().SetTitle("Tagging efficiency")
   teff_loose.GetPaintedGraph().Draw("AP")
   teff_medium.GetPaintedGraph().Draw("PSAME")
   teff_tight.GetPaintedGraph().Draw("PSAME")

   leg_height = 0.06
   leg_width = 0.275
   leg_xoffset = 0.6
   leg_yoffset = 0.85
   fLegend = TLegend(leg_xoffset,leg_yoffset,leg_xoffset+leg_width,leg_yoffset);
   fLegend.SetNColumns(1)
   fLegend.SetFillColor(0)
   fLegend.SetFillStyle(0)
   fLegend.SetBorderSize(0)
   fLegend.SetTextFont(72)
   fLegend.SetTextSize(0.04)
   fLegend.SetShadowColor(kWhite)
   fLegend.SetFillColor(kWhite)
   fLegend.SetLineColor(kWhite)
   fLegend.AddEntry(teff_loose.GetPaintedGraph(),        "loose WP","LP")
   fLegend.AddEntry(teff_medium.GetPaintedGraph(),       "medium WP","LP")
   fLegend.AddEntry(teff_tight.GetPaintedGraph(),        "tight WP","LP")
   fLegend.SetY1(fLegend.GetY1()-leg_height*fLegend.GetNRows())
   fLegend.Draw()
   ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.145);
   myText(0.195,0.825,1,"#bf{#sqrt{s}=13 TeV}")
   canv.SaveAs("Plots/FatElectronTaggingEfficiency_%s.pdf" % (Observable+"_"+Process))
