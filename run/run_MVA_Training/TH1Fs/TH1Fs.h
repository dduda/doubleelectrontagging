/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// Class similar to TH1Fch written by Chiara Rizzi.                        //
// This class provides an easier way to deal with histograms in pT bins.   //
// Author: luca.ambroz@cern.ch                                             //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////


#include "TH1F.h"
#include "TFile.h"
#include "TList.h"
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class TH1Fs {
  
    map<TString,TH1F*> m_histos_pt_int;
    TString m_name;
    TString m_title;
    vector<double> m_intervals;
    map<int,TString> m_interval_name_index;
 

 public:
    
    TH1Fs(TString histname, TString title, int nbins, double xlow, double xup, vector<double> intervals, vector<TString> Types){
        m_intervals = intervals;        
        //Histograms in pT bins
        vector<TString> names_pt_int;
        for(unsigned int i=0; i<intervals.size()-1; i++){
            TString int_name = ""; 
            int_name += intervals.at(i);
            if(i<intervals.size()-1) {
                int_name+="_";
                int_name+=intervals.at(i+1);
            }
            m_interval_name_index[i] = int_name; //Check i!!!!!!!!!!!
            names_pt_int.push_back(int_name);
        }
        TString appo_name_pt_int;
        for(int i=0; i<Types.size(); i++){
            for(int j=0; j<names_pt_int.size(); j++){
               std::cout<<i<<"  "<<j<<"  "<< names_pt_int.at(j)<<"   "<<Types.at(i)<<std::endl;
               appo_name_pt_int = histname + "_" + names_pt_int.at(j) + "_" + Types.at(i);
               TH1F * h_appo_pt_int =new TH1F(appo_name_pt_int,"", nbins, xlow, xup);
               m_histos_pt_int[appo_name_pt_int]=(TH1F*)h_appo_pt_int->Clone();
               m_histos_pt_int[appo_name_pt_int]->SetDefaultSumw2();
               delete h_appo_pt_int;
            }
        }
    }
    
  void Fill(double val, double weight, int int_index, TString m_Type);	
  void Write();
};
