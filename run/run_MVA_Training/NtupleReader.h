#ifndef NtupleReader_h
#define NtupleReader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TGraph.h>

#include "vector"

#include "TMVA/Factory.h"
#include "TMVA/Reader.h"
#include "TMVA/DataLoader.h"
#include "TH1Fs/TH1Fs.h"
#include "TH2Fs/TH2Fs.h"
#include "TEfficiencies/TEfficiencies.h"

using namespace std;

class NtupleReader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   vector<double> intervals; //!
   vector<TString> m_Types;
   vector<float> ROC_vector_sigPassed; //!
   vector<float> ROC_vector_QCDJetBkgPassed; //!
   vector<float> ROC_vector_gammaJetBkgPassed; //!
   vector<float> ROC_vector_SingleEJetBkgPassed; //!
   vector<float> ROC_vector_AllBkgPassed; //!
 
   TGraph    *ROC_signal_vs_QCDBkg; //!
   TGraph    *ROC_signal_vs_gammaJetBkg; //!
   TGraph    *ROC_signal_vs_SingleEJetBkg; //!

   // Fixed size dimensions of array or collections stored in the TTree if any.
   // Declaration of leaf types
   
   TMVA::Reader  *m_reader;  //!

   int   type; //!
   float weight; //!
   float m_BDTresponse; //!
   float FatElectron_M; //!
   float FatElectron_pT; //!
   float FatElectron_Eta; //!
   float FatElectron_EMFrac; //!
   float FatElectron_Eratio; //!
   float FatElectron_balance; //!
   float Trk1_dRToJet; //!
   float Trk2_dRToJet; //!
   float Trk1_d0; //!
   float Trk2_d0; //!
   float Trk1_d0Sig; //!
   float Trk2_d0Sig; //!
   float Trk1_z0; //!
   float Trk2_z0; //!
   float Trk1_z0Sig; //!
   float Trk2_z0Sig; //!
   float dR_tt; //!
   float Trk1_EOverP; //!
   float Trk2_EOverP; //!
   float Trk1_dPhi; //!
   float Trk2_dPhi; //!
   float Trk1_dEta; //!
   float Trk2_dEta; //!
   float Trk2_eProbabilityHT; //!
   float Trk1_eProbabilityHT; //!
   int   FatElectron_ntrks; //!
   int   ChargeSum; //!
   float   nVertices; //!

   // List of branches
   TBranch        *b_nVertices; //!
   TBranch        *b_type; //!
   TBranch        *b_weight; //!
   TBranch        *b_FatElectron_pT; //!
   TBranch        *b_FatElectron_M; //!
   TBranch        *b_FatElectron_Eta; //!
   TBranch	  *b_FatElectron_EMFrac; //!
   TBranch	  *b_FatElectron_Eratio; //!
   TBranch	  *b_FatElectron_balance; //!
   TBranch        *b_Trk1_dRToJet; //!
   TBranch	  *b_Trk2_dRToJet; //!
   TBranch        *b_Trk1_d0; //!
   TBranch        *b_Trk2_d0; //!
   TBranch        *b_Trk1_d0Sig; //!
   TBranch        *b_Trk2_d0Sig; //!
   TBranch        *b_Trk1_z0; //!
   TBranch        *b_Trk2_z0; //!
   TBranch        *b_Trk1_z0Sig; //!
   TBranch        *b_Trk2_z0Sig; //!
   TBranch        *b_Trk1_EOverP; //!
   TBranch	  *b_Trk2_EOverP; //!
   TBranch	  *b_Trk1_dPhi; //!
   TBranch        *b_Trk2_dPhi; //!
   TBranch	  *b_Trk2_dEta; //!
   TBranch	  *b_Trk1_dEta; //!
   TBranch        *b_Trk2_eProbabilityHT; //!
   TBranch	  *b_Trk1_eProbabilityHT; //!
   TBranch        *b_FatElectron_ntrks; //!
   TBranch        *b_dR_tt; //!
   TBranch        *b_ChargeSum; //!

   NtupleReader(TTree *tree=0);
   virtual ~NtupleReader();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   void SetROCVector(int nbins);
   void FillROCVector(std::vector<float> &ROC_vector, float BDTresponse, float weight, int nbins);
   void PrepareTrainingsInput(TTree *output_Tree);
   void TrainMVA();
   void Write(TString name);
   void initialize();
   float EvaluateMVAResponse();

   ////General kinematic variables////
   TH1Fs *h_BDTresponse;
   TH1Fs *h_D0;
   TH1Fs *h_Z0;
   TH1Fs *h_FatElectron_M;
   TH1Fs *h_FatElectron_EMFrac;
   TH1Fs *h_FatElectron_pT;
   TH1Fs *h_FatElectron_Eratio;
   TH1Fs *h_FatElectron_balance;
   TH1Fs *h_Trk1_dRToJet;
   TH1Fs *h_Trk2_dRToJet;
   TH1Fs *h_Ntrks;
   TH1Fs *h_Trk1_d0;
   TH1Fs *h_Trk2_d0;
   TH1Fs *h_Trk1_z0;
   TH1Fs *h_Trk2_z0;
   TH1Fs *h_Trk1_d0Sig;
   TH1Fs *h_Trk2_d0Sig;
   TH1Fs *h_Trk1_z0Sig; 
   TH1Fs *h_Trk2_z0Sig; 
   TH1Fs *h_Trk1_EOverP;
   TH1Fs *h_Trk2_EOverP;
   TH1Fs *h_Trk1_dPhi;
   TH1Fs *h_Trk2_dPhi;
   TH1Fs *h_Trk1_dEta;
   TH1Fs *h_Trk2_dEta;
   TH1Fs *h_Trk1_eProbabilityHT;
   TH1Fs *h_Trk2_eProbabilityHT;
   TH1Fs *h_DD_tracks;

   TH2Fs *h_FatElectron_balance_vs_FatElectron_EMFrac;
   TH2Fs *h_FatElectron_balance_vs_FatElectron_ntrks;
   TH2Fs *h_FatElectron_balance_vs_Trk1_dRToJet;
   TH2Fs *h_FatElectron_balance_vs_Trk2_dRToJet;
   TH2Fs *h_FatElectron_EMFrac_vs_FatElectron_ntrks;
   TH2Fs *h_FatElectron_EMFrac_vs_Trk1_dRToJet;
   TH2Fs *h_FatElectron_EMFrac_vs_Trk2_dRToJet;
   TH2Fs *h_FatElectron_ntrks_vs_Trk1_dRToJet;
   TH2Fs *h_FatElectron_ntrks_vs_Trk2_dRToJet;
   TH2Fs *h_Trk1_dRToJet_vs_Trk2_dRToJet;

   TH2Fs *h_FatElectron_M_vs_FatElectron_EMFrac;
   TH2Fs *h_FatElectron_M_vs_FatElectron_balance;
   TH2Fs *h_FatElectron_M_vs_FatElectron_ntrks;
   TH2Fs *h_FatElectron_M_vs_Trk1_dRToJet;
   TH2Fs *h_FatElectron_M_vs_Trk2_dRToJet;

   TEfficiencies* efficiency_loose_vs_pT;
   TEfficiencies* efficiency_medium_vs_pT;
   TEfficiencies* efficiency_tight_vs_pT;

   TEfficiencies* efficiency_loose_vs_Eta;
   TEfficiencies* efficiency_medium_vs_Eta;
   TEfficiencies* efficiency_tight_vs_Eta;

   TEfficiencies* efficiency_loose_vs_nVtx;
   TEfficiencies* efficiency_medium_vs_nVtx;
   TEfficiencies* efficiency_tight_vs_nVtx;

   float in_FatElectron_ntrks;
   float in_FatElectron_EMFrac;
   float in_FatElectron_balance;
   float in_Trk1_dRToJet;
   float in_Trk2_dRToJet;
   float in_Trk1_EOverP;
   float in_Trk2_EOverP;
   float in_Trk1_dPhi;
   float in_Trk2_dPhi;
   float in_Trk1_dEta;
   float in_Trk2_dEta;
   float in_dR_tt;  
   float in_ChargeSum;
   float in_FatElectron_M;

};

#endif

#ifdef NtupleReader_cxx
NtupleReader::NtupleReader(TTree *tree) : fChain(0) {
   Init(tree);
}

NtupleReader::~NtupleReader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t NtupleReader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t NtupleReader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void NtupleReader::Init(TTree *tree)
{
   intervals = { 300, 500, 1000, 2000};
   m_Types   = {"Zee","QCDjets","Photonjets","ejets"};
   h_BDTresponse = new TH1Fs("h_BDTresponse","",40,-1,1,intervals,m_Types);
   h_Z0       = new TH1Fs("h_z0","",40,0,20,intervals,m_Types);
   h_D0       = new TH1Fs("h_d0","",40,0,6,intervals,m_Types);
   h_FatElectron_M = new TH1Fs("h_FatElectron_M","",40, 0, 300,intervals,m_Types);
   h_Ntrks    = new TH1Fs("h_Ntrks","",14,0.5,14.5,intervals,m_Types);
   h_FatElectron_EMFrac = new TH1Fs("FatElectron_EMFrac","",40,0.2,1,intervals,m_Types);
   h_FatElectron_Eratio = new TH1Fs("FatElectron_Eratio","",40,0,1,intervals,m_Types);
   h_FatElectron_balance = new TH1Fs("h_FatElectron_balance","",40,0,1,intervals,m_Types);
   h_Trk1_dRToJet = new TH1Fs("h_Trk1_dRToJet","",40,0.0,0.4,intervals,m_Types);
   h_Trk2_dRToJet = new TH1Fs("h_Trk2_dRToJet","",40,0.0,0.45,intervals,m_Types);
   h_Trk1_d0 = new TH1Fs("h_Trk1_d0","",40,0.0,0.18,intervals,m_Types);
   h_Trk2_d0 = new TH1Fs("h_Trk2_d0","",40,0.0,0.4,intervals,m_Types);
   h_Trk1_z0 = new TH1Fs("h_Trk1_z0","",40,0.0,0.55,intervals,m_Types);
   h_Trk2_z0 = new TH1Fs("h_Trk2_z0","",40,0.0,0.5,intervals,m_Types);
   h_Trk1_d0Sig = new TH1Fs("h_Trk1_d0Sig","",40,0,14,intervals,m_Types);
   h_Trk2_d0Sig = new TH1Fs("h_Trk2_d0Sig","",40,0,11,intervals,m_Types);
   h_Trk1_z0Sig = new TH1Fs("h_Trk1_z0Sig","",40,0,7,intervals,m_Types);
   h_Trk2_z0Sig = new TH1Fs("h_Trk2_z0Sig","",40,0,7,intervals,m_Types);
   h_Trk2_EOverP = new TH1Fs("h_Trk2_EOverP","",40,0,20,intervals,m_Types);
   h_Trk1_EOverP = new TH1Fs("h_Trk1_EOverP","",40,0,20,intervals,m_Types);
   h_Trk1_dPhi = new TH1Fs("h_Trk1_dPhi","",40,0.0,0.1,intervals,m_Types);
   h_Trk2_dPhi = new TH1Fs("h_Trk2_dPhi","",40,0.0,0.1,intervals,m_Types);
   h_Trk1_dEta = new TH1Fs("h_Trk1_dEta","",40,0.0,0.09,intervals,m_Types);
   h_Trk2_dEta = new TH1Fs("h_Trk2_dEta","",40,0.0,0.11,intervals,m_Types);
   h_Trk1_eProbabilityHT = new TH1Fs("Trk1_eProbabilityHT","",40,0.0,1.0,intervals,m_Types);
   h_Trk2_eProbabilityHT = new TH1Fs("Trk2_eProbabilityHT","",40,0.0,1.0,intervals,m_Types);
   h_DD_tracks= new TH1Fs("h_DR_tracks","",40,0.0,0.45,intervals,m_Types);

   h_FatElectron_M_vs_FatElectron_EMFrac = new TH2Fs("FatElectron_M_vs_FatElectron_EMFrac","",40, 0, 300, 40, 0, 1, m_Types,  intervals);
   h_FatElectron_M_vs_FatElectron_balance = new TH2Fs("FatElectron_M_vs_FatElectron_balance","",40, 0, 300, 40, 0.2, 1, m_Types,  intervals);
   h_FatElectron_M_vs_FatElectron_ntrks = new TH2Fs("FatElectron_M_vs_FatElectron_ntrks","",40, 0, 300, 14, 0.5, 14.5, m_Types,  intervals);
   h_FatElectron_M_vs_Trk1_dRToJet = new TH2Fs("FatElectron_M_vs_Trk1_dRToJet","",40, 0, 300, 40, 0, 0.4, m_Types,  intervals);
   h_FatElectron_M_vs_Trk2_dRToJet = new TH2Fs("FatElectron_M_vs_Trk2_dRToJet","",40, 0, 300, 40, 0, 0.45, m_Types,  intervals);

   h_FatElectron_balance_vs_FatElectron_EMFrac = new TH2Fs("FatElectron_balance_vs_FatElectron_EMFrac", "",40, 0.2, 1 ,40, 0, 1, m_Types,  intervals);
   h_FatElectron_balance_vs_FatElectron_ntrks = new TH2Fs("h_FatElectron_balance_vs_FatElectron_ntrks","",40, 0.2, 1, 14, 0.5, 14.5, m_Types,  intervals);
   h_FatElectron_balance_vs_Trk1_dRToJet = new TH2Fs("h_FatElectron_balance_vs_Trk1_dRToJet","",40, 0.2,1, 40, 0, 0.4, m_Types,  intervals);
   h_FatElectron_balance_vs_Trk2_dRToJet = new TH2Fs("h_FatElectron_balance_vs_Trk2_dRToJet","",40, 0.2,1, 40, 0, 0.45, m_Types,  intervals);
   h_FatElectron_EMFrac_vs_FatElectron_ntrks = new TH2Fs("h_FatElectron_EMFrac_vs_FatElectron_ntrks","",40, 0, 1, 14, 0.5, 14.5, m_Types,  intervals);
   h_FatElectron_EMFrac_vs_Trk1_dRToJet = new TH2Fs("h_FatElectron_EMFrac_vs_Trk1_dRToJet","",40, 0, 1, 40, 0, 0.4, m_Types,  intervals);
   h_FatElectron_EMFrac_vs_Trk2_dRToJet = new TH2Fs("h_FatElectron_EMFrac_vs_Trk2_dRToJet","",40, 0, 1, 40, 0, 0.45, m_Types,  intervals);
   h_FatElectron_ntrks_vs_Trk1_dRToJet = new TH2Fs("h_FatElectron_ntrks_vs_Trk1_dRToJet","",14, 0.5, 14.5, 40, 0, 0.4, m_Types,  intervals);
   h_FatElectron_ntrks_vs_Trk2_dRToJet = new TH2Fs("h_FatElectron_ntrks_vs_Trk2_dRToJet","",14, 0.5, 14.5, 40, 0, 0.45, m_Types,  intervals);
   h_Trk1_dRToJet_vs_Trk2_dRToJet = new TH2Fs("h_Trk1_dRToJet_vs_Trk2_dRToJet","",40, 0, 0.4, 40, 0, 0.45, m_Types,  intervals);


   double *pT_bins=new double[11];
   pT_bins[0]=300;
   pT_bins[1]=400;
   pT_bins[2]=500;
   pT_bins[3]=600;
   pT_bins[4]=750;
   pT_bins[5]=1000;
   pT_bins[6]=1250;
   pT_bins[7]=1500;
   pT_bins[8]=2000;
   pT_bins[9]=3000;
   pT_bins[10]=5000;

   double *eta_bins=new double[11];
   eta_bins[0]=0.00;
   eta_bins[1]=0.25;
   eta_bins[2]=0.50;
   eta_bins[3]=0.75;
   eta_bins[4]=1.00;
   eta_bins[5]=1.25;
   eta_bins[6]=1.50;
   eta_bins[7]=1.75;
   eta_bins[8]=2.00;
   eta_bins[9]=2.25;
   eta_bins[10]=2.5;

   double *nVtx_bins=new double[11];
   nVtx_bins[0]  = 4;
   nVtx_bins[1]  = 8;
   nVtx_bins[2]  = 12;
   nVtx_bins[3]  = 16;
   nVtx_bins[4]  = 20;
   nVtx_bins[5]  = 24;
   nVtx_bins[6]  = 28;
   nVtx_bins[7]  = 32;
   nVtx_bins[8]  = 38;
   nVtx_bins[9]  = 44;
   nVtx_bins[10] = 50;

   efficiency_loose_vs_pT    = new TEfficiencies("Efficiency_vs_pT_loose","",10,pT_bins,m_Types);
   efficiency_loose_vs_Eta   = new TEfficiencies("Efficiency_vs_Eta_loose","",10,eta_bins,m_Types);
   efficiency_loose_vs_nVtx  = new TEfficiencies("Efficiency_vs_nVtx_loose","",10,nVtx_bins,m_Types);

   efficiency_medium_vs_pT    = new TEfficiencies("Efficiency_vs_pT_medium","",10,pT_bins,m_Types);
   efficiency_medium_vs_Eta   = new TEfficiencies("Efficiency_vs_Eta_medium","",10,eta_bins,m_Types);
   efficiency_medium_vs_nVtx  = new TEfficiencies("Efficiency_vs_nVtx_medium","",10,nVtx_bins,m_Types);

   efficiency_tight_vs_pT    = new TEfficiencies("Efficiency_vs_pT_tight","",10,pT_bins,m_Types);
   efficiency_tight_vs_Eta   = new TEfficiencies("Efficiency_vs_Eta_tight","",10,eta_bins,m_Types);
   efficiency_tight_vs_nVtx  = new TEfficiencies("Efficiency_vs_nVtx_tight","",10,nVtx_bins,m_Types);

   ROC_signal_vs_QCDBkg         = new TGraph();
   ROC_signal_vs_QCDBkg->SetName("ROC_signal_vs_QCDBkg");
   ROC_signal_vs_gammaJetBkg    = new TGraph();
   ROC_signal_vs_gammaJetBkg->SetName("ROC_signal_vs_gammaJetBkg");
   ROC_signal_vs_SingleEJetBkg  = new TGraph();
   ROC_signal_vs_SingleEJetBkg->SetName("ROC_signal_vs_SingleEJetBkg");

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   fChain->SetBranchAddress("nVertices", &nVertices,   &b_nVertices);
   fChain->SetBranchAddress("type", &type,   &b_type);
   fChain->SetBranchAddress("weight", &weight,   &b_weight);
   fChain->SetBranchAddress("FatElectron_M", &FatElectron_M,  &b_FatElectron_M);
   fChain->SetBranchAddress("FatElectron_pT", &FatElectron_pT,  &b_FatElectron_pT);
   fChain->SetBranchAddress("FatElectron_Eta", &FatElectron_Eta,  &b_FatElectron_Eta);
   fChain->SetBranchAddress("FatElectron_EMFrac", &FatElectron_EMFrac,  &b_FatElectron_EMFrac);
   fChain->SetBranchAddress("FatElectron_Eratio", &FatElectron_Eratio,  &b_FatElectron_Eratio);
   fChain->SetBranchAddress("FatElectron_balance", &FatElectron_balance,  &b_FatElectron_balance);
   fChain->SetBranchAddress("Trk1_dRToJet", &Trk1_dRToJet,  &b_Trk1_dRToJet);
   fChain->SetBranchAddress("Trk2_dRToJet", &Trk2_dRToJet,  &b_Trk2_dRToJet);
   fChain->SetBranchAddress("Trk1_d0", &Trk1_d0,   &b_Trk1_d0);
   fChain->SetBranchAddress("Trk2_d0", &Trk2_d0,   &b_Trk2_d0);
   fChain->SetBranchAddress("Trk1_z0", &Trk1_z0,   &b_Trk1_z0);
   fChain->SetBranchAddress("Trk2_z0", &Trk2_z0,   &b_Trk2_z0);
   fChain->SetBranchAddress("Trk1_d0Sig", &Trk1_d0Sig,   &b_Trk1_d0Sig);
   fChain->SetBranchAddress("Trk2_d0Sig", &Trk2_d0Sig,   &b_Trk2_d0Sig);
   fChain->SetBranchAddress("Trk1_z0Sig", &Trk1_z0Sig,   &b_Trk1_z0Sig);
   fChain->SetBranchAddress("Trk2_z0Sig", &Trk2_z0Sig,   &b_Trk2_z0Sig);
   fChain->SetBranchAddress("dR_tt", &dR_tt,   &b_dR_tt);
   fChain->SetBranchAddress("Trk1_EOverP", &Trk1_EOverP,  &b_Trk1_EOverP);
   fChain->SetBranchAddress("Trk2_EOverP", &Trk2_EOverP,  &b_Trk2_EOverP);
   fChain->SetBranchAddress("Trk1_dPhi", &Trk1_dPhi,  &b_Trk1_dPhi);
   fChain->SetBranchAddress("Trk2_dPhi", &Trk2_dPhi,  &b_Trk2_dPhi);
   fChain->SetBranchAddress("Trk1_dEta", &Trk1_dEta,  &b_Trk1_dEta);
   fChain->SetBranchAddress("Trk2_dEta", &Trk2_dEta,  &b_Trk2_dEta);
   fChain->SetBranchAddress("Trk1_eProbabilityHT", &Trk1_eProbabilityHT,  &b_Trk1_eProbabilityHT);
   fChain->SetBranchAddress("Trk2_eProbabilityHT", &Trk2_eProbabilityHT,  &b_Trk2_eProbabilityHT);
   fChain->SetBranchAddress("FatElectron_ntrks", &FatElectron_ntrks,   &b_FatElectron_ntrks);
   fChain->SetBranchAddress("ChargeSum", &ChargeSum,   &b_ChargeSum);
   Notify();
}

Bool_t NtupleReader::Notify()
{
   return kTRUE;
}

void NtupleReader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}

Int_t NtupleReader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

#endif // #ifdef NtupleReader_cxx
