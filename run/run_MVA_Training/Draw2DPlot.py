# -*- coding: utf-8 -*- python
import sys
import glob
import math
from ROOT import *
from array import *

gROOT.LoadMacro("../AtlasStyle.C")
gROOT.LoadMacro("../AtlasUtils.C")
SetAtlasStyle()

YAxisTitle = {}
XAxisTitle = {}

YAxisTitle["h_Trk1_dRToJet_vs_Trk2_dRToJet"] = "#DeltaR(trk_{1},jet)"
XAxisTitle["h_Trk1_dRToJet_vs_Trk2_dRToJet"] = "#DeltaR(trk_{2},jet)"

def NormalizeHisto(histo,HistName):
     sumOfWeights = histo.GetSumOfWeights()
     histo.Scale(1.0/sumOfWeights)
     histo.SetStats(0)
     HName = HistName
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(506)
     histo.GetXaxis().SetNdivisions(506)
     histo.GetXaxis().SetTitle(XAxisTitle[HName])
     histo.GetYaxis().SetTitle(YAxisTitle[HName])

file   =  TFile.Open("FatElectronHistos.root","READ")
###file.ls()

for HistoName in ["h_Trk1_dRToJet_vs_Trk2_dRToJet"]:
  for PT_range in ["1000_2000"]:
     for Process in ["QCDjets"]:
       canv = TCanvas("canv_"+HistoName,"",720,720)
       canv.SetBatch(True)
       canv.SetCanvasSize(canv.GetWw(), canv.GetWh())
       canv.SetLogz()
       canv.SetRightMargin(0.16);
       canv.SetBottomMargin(0.16);
       canv.SetLeftMargin(0.16);

       h_2d = file.Get(HistoName+"_"+PT_range+"_"+Process) 
       NormalizeHisto(h_2d,HistoName)
       h_2d.Draw("COLZ")

       ATLAS_LABEL(0.185,0.885," work in progress",1,0.19)
       pT_Range = PT_range.replace("_",",")    
       myText(0.18,0.825,1,"#sqrt{s}=13 TeV   p_{T} #in  [" + pT_Range + "] GeV",0.04)

       canv.SaveAs("Plots/2DComparison_%s.pdf" %(HistoName+"_"+PT_range+"_"+Process))

