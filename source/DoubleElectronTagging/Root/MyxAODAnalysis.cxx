#include <DoubleElectronTagging/MyxAODAnalysis.h>

#include <xAODJet/JetContainer.h>
#include "xAODTracking/VertexContainer.h"
#include "xAODEgamma/ElectronContainer.h"

#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"

#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

#include "xAODBTagging/BTagging.h"
#include "xAODBTagging/BTaggingContainer.h"

#include "xAODEventInfo/EventInfo.h" 
#include "TMVA/Reader.h"

#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h" 
#include "xAODMissingET/MissingETContainer.h" 
#include "xAODMissingET/MissingETAuxContainer.h" 
#include "xAODMissingET/MissingETComposition.h"

using namespace std;

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.
}

StatusCode MyxAODAnalysis :: initialize (){
  
  m_eventCounter = 0;
  ANA_CHECK (book (TTree ("FatElectron", "")));
  m_myTree = tree ("FatElectron");
  m_myTree->Branch("FatElectron_pT",    &m_FatElectron_pT);
  m_myTree->Branch("FatElectron_Eta",   &m_FatElectron_Eta);
  m_myTree->Branch("FatElectron_M",     &m_FatElectron_M);
  m_myTree->Branch("FatElectron_ntrks", &m_FatElectron_ntrks);
  m_myTree->Branch("FatElectron_balance", &m_FatElectron_balance);
  m_myTree->Branch("FatElectron_EMFrac", &m_FatElectron_EMFrac);
  m_myTree->Branch("Trk1_dRToJet",      &m_Trk1_dRToJet);
  m_myTree->Branch("Trk2_dRToJet",      &m_Trk2_dRToJet);
  m_myTree->Branch("Trk1_d0",           &m_Trk1_d0);
  m_myTree->Branch("Trk2_d0",           &m_Trk2_d0);
  m_myTree->Branch("Trk1_d0Sig",        &m_Trk1_d0Sig);
  m_myTree->Branch("Trk2_d0Sig",        &m_Trk2_d0Sig);
  m_myTree->Branch("Trk1_z0",           &m_Trk1_z0);
  m_myTree->Branch("Trk2_z0",           &m_Trk2_z0);
  m_myTree->Branch("Trk1_z0Sig",        &m_Trk1_z0Sig);
  m_myTree->Branch("Trk2_z0Sig",        &m_Trk2_z0Sig);
  m_myTree->Branch("Trk1_EOverP",       &m_Trk1_EOverP);
  m_myTree->Branch("Trk2_EOverP",       &m_Trk2_EOverP);
  m_myTree->Branch("Trk1_dPhi",	        &m_Trk1_dPhi);
  m_myTree->Branch("Trk2_dPhi",	        &m_Trk2_dPhi);
  m_myTree->Branch("Trk1_dEta",	        &m_Trk1_dEta);
  m_myTree->Branch("Trk2_dEta",	        &m_Trk2_dEta);
  m_myTree->Branch("Trk1_eProbabilityHT", &m_Trk1_eProbabilityHT);
  m_myTree->Branch("Trk2_eProbabilityHT", &m_Trk2_eProbabilityHT);
  m_myTree->Branch("ChargeSum",           &m_ChargeSum);
  m_myTree->Branch("dR_tt",               &m_dR_tt);
  m_myTree->Branch("nVertices",           &m_nVertices);
  m_myTree->Branch("type",                &m_type);

   std::string confDir = "ElectronPhotonSelectorTools/offline/mc15_20160512/";

   m_ELIso = new CP::IsolationSelectionTool("ElectronIso");
   m_ELIso->setProperty("ElectronWP", "FCLoose");
   m_ELIso->initialize();
   m_ELIso->addElectronWP("FCTight");
   m_ELIso->addElectronWP("Gradient");
   m_ELIso->addElectronWP("FCTight_FixedRad");
   m_ELIso->addElectronWP("FCLoose_FixedRad");

   double *pT_bins=new double[11];
   pT_bins[0]=500;
   pT_bins[1]=550;
   pT_bins[2]=600;
   pT_bins[3]=700;
   pT_bins[4]=800;
   pT_bins[5]=1000;
   pT_bins[6]=1400;
   pT_bins[7]=2000;
   pT_bins[8]=3000;
   pT_bins[9]=5000;

   book (TH1F ("DoubleElectronLooseID_vs_ZpT", "", 9, pT_bins));
   book (TH1F ("DoubleElectronMediumID_vs_ZpT", "", 9, pT_bins));
   book (TH1F ("DoubleElectronTightID_vs_ZpT", "", 9, pT_bins));
   book (TH1F ("ZpT", "", 9, pT_bins));


  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute (){
      return FindDoubleElectronCandidate();
}

StatusCode MyxAODAnalysis :: finalize ()
{
  cout << "End. We have processed " << m_eventCounter << " events" << endl;
  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: FindDoubleElectronCandidate(){

  float pVZ              = 0;
  int nVertices          = 0;
  const xAOD::VertexContainer* vertices = 0;
  ANA_CHECK(evtStore()->retrieve(vertices, "PrimaryVertices"));
  for (auto vertex : *vertices) {
       if(nVertices==0){
         pVZ = vertex->z();
       }
       nVertices++;
   }

  ///std::cout<<"##############################################"<<std::endl;
  std::vector<TLorentzVector> Electrons;
  std::vector<TLorentzVector> Photons;
  std::vector<TLorentzVector> HF_hadrons;
  const xAOD::TruthParticleContainer* truthParticles = 0;
  ANA_CHECK(evtStore()->retrieve(truthParticles, "TruthParticles"));
  for (auto truthParticle : *truthParticles) {
      int pdgId   = fabs(truthParticle->pdgId());
      int status  = fabs(truthParticle->status());
      if(pdgId == 55 || pdgId/100 == 5 || pdgId/1000 == 5 || pdgId == 44 || pdgId/100 == 4 || pdgId/1000 == 4){
           HF_hadrons.push_back(truthParticle->p4());
      }   
      if(pdgId == 11 && truthParticle->status() == 3 /*or 11 ??*/){
           Electrons.push_back(truthParticle->p4());
      }
      if(pdgId == 22 && truthParticle->status() == 3){
           Photons.push_back(truthParticle->p4());
      }
  }
  int   nPhotons             = Photons.size();
  int   nElectrons           = Electrons.size();
  float dR_between_electrons = 0;  
  std::vector<TLorentzVector> *GSF_Tracks  = new std::vector<TLorentzVector>;
  std::vector<int>            *GSF_Indices = new std::vector<int>;
  std::vector<float>          *GSF_d0    = new std::vector<float>;
  std::vector<float>          *GSF_d0Sig = new std::vector<float>;
  std::vector<float>          *GSF_z0    = new std::vector<float>;
  std::vector<float>          *GSF_z0Sig = new std::vector<float>;
  std::vector<float>          *GSF_eProbabilityHT = new std::vector<float>;
  std::vector<float>          *GSF_charge = new std::vector<float>;
  std::vector<float>          *GSF_qOverP = new std::vector<float>;
  const xAOD::TrackParticleContainer* GSFTracks = 0;
  ANA_CHECK(evtStore()->retrieve(GSFTracks, "GSFTrackParticles"));
  ////std::cout<<"#############################"<<std::endl;
  int nGSFTracks = 0;
  bool found_ElectronJet = false;
  TLorentzVector ElectronJet;
  const xAOD::JetContainer* jets = 0;
  ANA_CHECK(evtStore()->retrieve( jets, "AntiKt4EMPFlowJets" ));
  for (auto jet : *jets) {
      if(jet->pt()*0.001 < 300. || std::fabs(jet->eta()) > 4.5)continue;
      if((nPhotons != 1 && nElectrons == 2 && ((jet->p4().DeltaR(Electrons.at(0)) < 0.4 && jet->p4().DeltaR(Electrons.at(1)) > 0.4) || (jet->p4().DeltaR(Electrons.at(0)) > 0.4 && jet->p4().DeltaR(Electrons.at(1)) < 0.4))))continue;
      bool has_HF_hadron = false;
      for(auto HF_hadron : HF_hadrons){
          if(jet->p4().DeltaR(HF_hadron) < 0.4)has_HF_hadron = true;
      }
      m_type = ((nElectrons == 2 && jet->p4().DeltaR(Electrons.at(0)) < 0.4 && jet->p4().DeltaR(Electrons.at(1)) < 0.4) ? 1 : (has_HF_hadron ? 5 : ( (nPhotons == 1 && Photons.at(0).DeltaR(jet->p4()) < 0.4) ? 3 : ((nElectrons == 1 && jet->p4().DeltaR(Electrons.at(0)) < 0.4) ? 7 : 6)))); 
      ElectronJet       = jet->p4();
      found_ElectronJet = true;  
      nGSFTracks        = 0;
      GSF_Tracks->clear();
      GSF_Indices->clear(); 
      GSF_d0->clear();
      GSF_d0Sig->clear();
      GSF_z0->clear();
      GSF_z0Sig->clear();
      GSF_eProbabilityHT->clear();
      GSF_charge->clear();
      GSF_qOverP->clear();;
      for (auto GSFTrack : *GSFTracks) {
           float dR_toJet = ElectronJet.DeltaR(GSFTrack->p4()); 
           if(dR_toJet < 0.4){
                  xAOD::ParametersCovMatrix_t cov = GSFTrack->definingParametersCovMatrix();
                  float d0           = GSFTrack->d0();
                  float z0sinTheta   = (GSFTrack->z0() + GSFTrack->vz() - pVZ)*TMath::Sin(GSFTrack->theta());
                  float d0_err       = TMath::Sqrt(cov(0, 0));
                  float z0_err       = TMath::Sqrt(cov(1, 1));
                  int   charge       = GSFTrack->qOverP()/fabs(GSFTrack->qOverP());
                  uint8_t numberOfBLHits(0),numberOfIBLHits(0),numberOfPixelHits(0),numberOfSCTHits(0), numberOfTRTHits(0);
                  float eProbabilityHT(0);
                  GSFTrack->summaryValue(numberOfIBLHits,xAOD::numberOfInnermostPixelLayerHits);
                  GSFTrack->summaryValue(numberOfBLHits,xAOD::numberOfNextToInnermostPixelLayerHits);
                  GSFTrack->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits);
                  GSFTrack->summaryValue(numberOfSCTHits,xAOD::numberOfSCTHits);
                  GSFTrack->summaryValue(numberOfTRTHits,xAOD::numberOfTRTHits);
                  GSFTrack->summaryValue(eProbabilityHT,xAOD::SummaryType::eProbabilityHT);

                  if(GSFTrack->p4().Pt()*0.001 < 1.0 || fabs(z0sinTheta)>1.5 || fabs(d0) > 1.0 || ((int) numberOfSCTHits + (int) numberOfPixelHits) < 7 || ((int) numberOfIBLHits) < 1){
                     continue;
                  }
                  GSF_Indices->push_back(nGSFTracks);
                  GSF_Tracks->push_back(GSFTrack->p4());
                  GSF_d0->push_back(fabs(d0));
                  GSF_d0Sig->push_back(fabs(d0/d0_err));
                  GSF_z0->push_back(fabs(z0sinTheta));
                  GSF_z0Sig->push_back(fabs(z0sinTheta/z0_err));
                  GSF_eProbabilityHT->push_back(eProbabilityHT);
                  GSF_charge->push_back(charge);
                  GSF_qOverP->push_back(GSFTrack->qOverP());
                  nGSFTracks++;
          }
      }
      Sort_trks(GSF_Tracks, GSF_Indices);
      std::vector<float>  *ECand_EOverP = new std::vector<float>;
      std::vector<float>  *ECand_dEta = new std::vector<float>;
      std::vector<float>  *ECand_dPhi = new std::vector<float>;

      int index(-1), index_I(-1), index_II(-1);
      float cluster_e(0.), dEta(0.), dPhi(0.),EnergySumTrks(0.),EnergySumJet(0.),EnergyDifferenceTrks(0.);
      const xAOD::CaloClusterContainer* clusters = 0;
      ANA_CHECK(evtStore()->retrieve(clusters, "egammaClusters"));
      for(unsigned int i = 0; i < (GSF_Tracks->size() < 2 ? GSF_Tracks->size() : 2); i++){
         float dRmin = 99;
         int nClusters = 0;
         for (auto cluster : *clusters) {
            float dR = GSF_Tracks->at(i).DeltaR(cluster->p4());
            if(dRmin > dR){
               dRmin     = dR;
               index     = nClusters; 
               dEta      = fabs(cluster->eta()-GSF_Tracks->at(i).Eta());
               dPhi      = fabs(GSF_Tracks->at(i).DeltaPhi(cluster->p4()));
               cluster_e = (cluster->clusterSize() != xAOD::CaloCluster::SW_35ele && cluster->clusterSize() != xAOD::CaloCluster::SW_35gam ) ?  cluster->altE() : cluster->calE();
            }
            float dRToJet  = ElectronJet.DeltaR(cluster->p4());
            if(i == 0 && dRToJet<0.4){
                EnergySumJet += (cluster->clusterSize() != xAOD::CaloCluster::SW_35ele && cluster->clusterSize() != xAOD::CaloCluster::SW_35gam ) ?  cluster->altE() : cluster->calE();     
            }
            nClusters++;
         }
         if(i==0){
            EnergyDifferenceTrks += cluster_e;
            index_I  = index; 
         }else{
            EnergyDifferenceTrks -= cluster_e;
            index_II = index;
         }
         if(index_II != index_I)EnergySumTrks += cluster_e;
         ECand_EOverP->push_back(fabs(cluster_e*GSF_qOverP->at(GSF_Indices->at(i))));
         ECand_dEta->push_back(dEta);
         ECand_dPhi->push_back(dPhi); 
      }
      m_FatElectron_pT       = ElectronJet.Pt()*0.001;
      m_FatElectron_EMFrac   = jet->auxdata<float>("EMFrac");
      m_FatElectron_Eta      = ElectronJet.Eta();
      m_FatElectron_M        = ElectronJet.M()*0.001;
      m_FatElectron_ntrks    = nGSFTracks;
      m_nVertices            = nVertices;
      if(GSF_Tracks->size() >= 2){
         m_FatElectron_balance  = fabs(EnergyDifferenceTrks/EnergySumTrks);
         if(index_II == index_I) m_FatElectron_balance = -1.;
      }else{
         m_FatElectron_balance = -1.;
      }
      m_Trk1_dRToJet         = (GSF_Tracks->size() > 0 ? GSF_Tracks->at(0).DeltaR(ElectronJet) : 0.5);
      m_Trk2_dRToJet         = (GSF_Tracks->size() > 1 ? GSF_Tracks->at(1).DeltaR(ElectronJet) : 0.5);
      m_Trk1_d0              = (GSF_Tracks->size() > 0 ? GSF_d0->at(GSF_Indices->at(0)) : 1.0);
      m_Trk2_d0              = (GSF_Tracks->size() > 1 ? GSF_d0->at(GSF_Indices->at(1)) : 1.0);
      m_Trk1_d0Sig           = (GSF_Tracks->size() > 0 ? GSF_d0Sig->at(GSF_Indices->at(0)) : -1.0);
      m_Trk2_d0Sig           = (GSF_Tracks->size() > 1 ? GSF_d0Sig->at(GSF_Indices->at(1)) : -1.0);
      m_Trk1_z0              = (GSF_Tracks->size() > 0 ? GSF_z0->at(GSF_Indices->at(0))    : 1.5);
      m_Trk2_z0              = (GSF_Tracks->size() > 1 ? GSF_z0->at(GSF_Indices->at(1))    : 1.5);
      m_Trk1_z0Sig           = (GSF_Tracks->size() > 0 ? GSF_z0Sig->at(GSF_Indices->at(0)) : -1.0);
      m_Trk2_z0Sig           = (GSF_Tracks->size() > 1 ? GSF_z0Sig->at(GSF_Indices->at(1)) : -1.0);
      m_Trk1_eProbabilityHT  = (GSF_Tracks->size() > 0 ? GSF_eProbabilityHT->at(GSF_Indices->at(0)) : 0.0);
      m_Trk2_eProbabilityHT  = (GSF_Tracks->size() > 1 ? GSF_eProbabilityHT->at(GSF_Indices->at(1)) : 0.0);
      m_Trk1_EOverP          = (GSF_Tracks->size() > 0 ? ECand_EOverP->at(0) : -1.0);
      m_Trk2_EOverP          = (GSF_Tracks->size() > 1 ? ECand_EOverP->at(1) : -1.0);
      m_Trk1_dEta            = (GSF_Tracks->size() > 0 ? ECand_dEta->at(0)   :  0.2);
      m_Trk2_dEta            = (GSF_Tracks->size() > 1 ? ECand_dEta->at(1)   :  0.2);
      m_Trk1_dPhi            = (GSF_Tracks->size() > 0 ? ECand_dPhi->at(0)   :  0.2);
      m_Trk2_dPhi            = (GSF_Tracks->size() > 1 ? ECand_dPhi->at(1)   :  0.2);
      m_ChargeSum            = (GSF_Tracks->size() > 1 ? (int) (GSF_charge->at(GSF_Indices->at(0)) + GSF_charge->at(GSF_Indices->at(1))) : -3);
      m_dR_tt                = (GSF_Tracks->size() > 1 ? GSF_Tracks->at(0).DeltaR(GSF_Tracks->at(1)) : -0.1);
      tree ("FatElectron")->Fill();
  }

  int nRecoElectronsLooseID(0),nRecoElectronsMediumID(0),nRecoElectronsTightID(0);  
  const xAOD::ElectronContainer* electrons = 0;
  ANA_CHECK(evtStore()->retrieve(electrons, "Electrons"));
  for (auto electron : *electrons) {
       const xAOD::TrackParticle *trackPart = electron->trackParticle();
       if (!trackPart) continue;  
       float t_eta       = trackPart->eta();
       float d0          = trackPart->d0();
       float d0sig       = xAOD::TrackingHelpers::d0significance(trackPart);
       float z0          = trackPart->z0()+ trackPart->vz() - pVZ;
       float z0sinTheta  = z0*TMath::Sin(trackPart->theta());
       bool IP_accepted = (fabs(d0/d0sig)<5. && fabs(z0sinTheta)<0.5);
       float E           = electron->e();
       float ET          = E/cosh(t_eta)*0.001;
       float eta         = electron->eta();
       if(ET < 25.)continue;
       if(fabs(eta) > 2.47)continue;
       if(fabs(eta) > 1.37 && fabs(eta) < 1.52)continue;
       if(!IP_accepted)continue;
       int isLooseLH   =  (int) electron -> auxdata<char>("LHLoose"); 
       int isMediumLH  =  (int) electron -> auxdata<char>("LHMedium");
       int isTightLH   =  (int) electron -> auxdata<char>("LHTight");
       float caloIso   =  electron -> auxdata<float>("topoetcone20")/trackPart->pt();
       float trackIso  =  electron -> auxdata<float>("ptvarcone20")/trackPart->pt();

       if(!(caloIso < 0.06 && trackIso < 0.06))continue;
       if(isLooseLH == 1)nRecoElectronsLooseID +=1;
       if(isMediumLH == 1)nRecoElectronsMediumID +=1;
       if(isTightLH == 1)nRecoElectronsTightID +=1;
  }

  if(Electrons.size()>= 2){
      if(nRecoElectronsLooseID >= 2)hist("DoubleElectronLooseID_vs_ZpT")->Fill((Electrons.at(0)+Electrons.at(1)).Pt()*0.001);
      if(nRecoElectronsMediumID >= 2)hist("DoubleElectronMediumID_vs_ZpT")->Fill((Electrons.at(0)+Electrons.at(1)).Pt()*0.001);
      if(nRecoElectronsTightID >= 2)hist("DoubleElectronTightID_vs_ZpT")->Fill((Electrons.at(0)+Electrons.at(1)).Pt()*0.001);
      hist("ZpT")->Fill((Electrons.at(0)+Electrons.at(1)).Pt()*0.001);
  } 
  ////std::cout<<nRecoElectrons<<std::endl;

  return StatusCode::SUCCESS;
}


void MyxAODAnalysis :: Sort_trks(std::vector<TLorentzVector> *trksInJet, std::vector<int> *trksIndices){
  bool bDone = false;
    while (!bDone)
    {
     	bDone = true;
        for (unsigned int i = 0; i<trksInJet->size()-1 && trksInJet->size()>0; i++)
        {
            if ( trksInJet->at(i).Pt() < trksInJet->at(i+1).Pt())
            {
             	TLorentzVector tmp   = trksInJet->at(i);
                int tmp_int          = trksIndices->at(i);
                trksInJet->at(i)     = trksInJet->at(i+1);
                trksInJet->at(i+1)   = tmp;
                trksIndices->at(i)   = trksIndices->at(i+1);
                trksIndices->at(i+1) = tmp_int;
                bDone = false;
            }
	}
    }
}


