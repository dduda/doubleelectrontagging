#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364219.Sherpa_221_NNPDF30NNLO_Zee_PTV1000_E_CMS.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364227.Sherpa_221_NNPDF30NNLO_Wenu_PTV1000_E_CMS.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364547.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_1000_E_CMS.merge.AOD.e6068_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364218.Sherpa_221_NNPDF30NNLO_Zee_PTV500_1000.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.merge.AOD.e5299_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364219.Sherpa_221_NNPDF30NNLO_Zee_PTV1000_E_CMS.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
###inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364545.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_280_500.merge.AOD.e5938_e5984_s3126_r10201_r10210/'
###inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.merge.AOD.e5299_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364226.Sherpa_221_NNPDF30NNLO_Wenu_PTV500_1000.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364219.Sherpa_221_NNPDF30NNLO_Zee_PTV1000_E_CMS.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364218.Sherpa_221_NNPDF30NNLO_Zee_PTV500_1000.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
#inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364546.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_500_1000.merge.AOD.e5938_e5984_s3126_r10201_r10210/'
#inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364547.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_1000_E_CMS.merge.AOD.e6068_e5984_s3126_r10201_r10210/'
#inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364227.Sherpa_221_NNPDF30NNLO_Wenu_PTV1000_E_CMS.merge.AOD.e5626_e5984_s3126_r10201_r10210/'
#inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.merge.AOD.e5340_e5984_s3126_r10201_r10210/'
###inputFilePath   = '/ptmp/mpp/dduda/Samples/DxAODs/EGAM/mc16_13TeV.304709.Sherpa_CT10_Zqq_Pt1000.merge.AOD.e4692_e5984_s3126_r10201_r10210/'
ROOT.SH.ScanDir().filePattern( '*.root*' ).scan( sh, inputFilePath )
###ROOT.SH.ScanDir().filePattern( 'AOD.12975170._000104.pool.root.1' ).scan( sh, inputFilePath )

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
### job.options().setDouble( ROOT.EL.Job.optMaxEvents,)  

# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.

from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )  ### HbbTagging/
job.algsAdd( config )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
